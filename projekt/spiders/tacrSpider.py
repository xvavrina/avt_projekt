import scrapy
import re
import datetime
from urllib.parse import urlparse


def remove_html_tags(text):
    """Remove html tags from a string"""
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)

supportform = 'div.infobox div.infobox__row:nth-child(2) div.infobox__value::text'

class GrantsSpider(scrapy.Spider):
    name = 'tacr'
    allowed_domains = ['www.tacr.cz']
    start_urls = ['https://www.tacr.cz/programy-a-souteze/programy/']

    def parse(self, response):
        for link in response.css('a.programs__item::attr(href)'):
            yield response.follow(link.get(),callback=self.parse_items)

    def parse_items(self, response):
        for link in response.css('a.calls__item::attr(href)'):
            yield response.follow(link.get(),callback=self.parse_grants)

    def parse_grants(self, response):
        for grant in response.css('div.page__wrapper'):
            date = grant.css('div.infobox__bigvalue::text').get().replace(' ','').split('-')
            ################################################################################
            popis = list()
            p = grant.css('p').getall()
            for i in range(len(p)):
                if ("cíl" in p[i].lower() or "předmět" in p[i].lower()) and "http" not in p[i].lower():
                    popis.append(remove_html_tags(p[i]))
            ################################################################################
            urls = grant.css('a.files__item::attr(href)').getall()
            names = grant.css('.files__title::text').getall()
            dokumenty = list()
            for i in range(len(urls)):
                dok = {'url': urls[i], 'name': names[i]}
                dokumenty.append(dok)
            ################################################################################
            yield {
                'headline': grant.css('div.breadcrumbs__item:nth-child(2) a.breadcrumbs__link::text').get() + ' - ' + grant.css('h2.jumbo__title.jumbo__uppercase::text').get(),
                'description': popis,
                'retrieved_at': datetime.datetime.now(),
                'domain': urlparse(response.url).netloc,
                'url': response.url,
                'program': grant.css('div.breadcrumbs__item:nth-child(2) a.breadcrumbs__link::text').get(),
                'start_date': date[0].strip() if date else None,
                'deadline_date': date[1].strip() if date else None,
                'receivers': grant.css('div.infobox div.infobox__row:nth-child(5) div.infobox__value::text').get().split(", "),
                'supported_activities': None,
                'support_form': grant.css(supportform).get().replace("mil.","000 000") if "mil." in grant.css(supportform).get() else grant.css(supportform).get().replace("tis.","00 000") if "tis." in grant.css(supportform).get() else grant.css(supportform).get().replace("mld.","000 000 000") if "mld." in grant.css(supportform).get() else grant.css(supportform).get(),
                'restriction': None,
                'documents': dokumenty
            }